/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import * as React from 'react';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import CustomNavigationBar from './components/navigationBar';
import VCardsDetails from './screens/vCardsDetails';
import HomeScreen from './screens/home';
import VCardsAdd from './screens/vCardsAdd';
import Welcome from './screens/welcome';

const Stack = createNativeStackNavigator();

const theme = {
  ...DefaultTheme,
  roundness: 20,
  colors: {
    ...DefaultTheme.colors,
    primary: '#102b52',
    accent: '#f17629',
  },
};

const App: () => Node = () => {
  return (
    <NavigationContainer>
      <PaperProvider theme={theme}>
        <Stack.Navigator
          initialRouteName="Welcome"
          screenOptions={{
            header: props => <CustomNavigationBar {...props} />,
          }}>
          <Stack.Screen
            options={{headerShown: false}}
            name="Welcome"
            component={Welcome}
          />
          <Stack.Screen name="Home" component={HomeScreen} />
          <Stack.Screen name="vCardDetails" component={VCardsDetails} />
          <Stack.Screen name="vCardAdd" component={VCardsAdd} />
        </Stack.Navigator>
      </PaperProvider>
    </NavigationContainer>
  );
};

export default App;
