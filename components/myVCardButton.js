import * as React from 'react';
import {Button} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  button: {
    alignSelf: 'center',
    padding: 10,
    margin: 10,
  },
});

function MyVCardButton({disable, userData}) {
  let contactName;
  let contactRole;
  let contactVCard;
  if (!disable) {
    contactName = userData[0].contact_name;
    contactRole = userData[0].contact_role;
    contactVCard = userData[0].contact_vcard;
  }

  const navigation = useNavigation();
  function redirect() {
    navigation.push('vCardDetails', {
      contactName,
      contactRole,
      contactVCard,
    });
  }
  return (
    <Button
      style={styles.button}
      icon="card-account-details"
      color="#D9BE96"
      mode="contained"
      onPress={() => redirect()}
      disabled={disable}>
      Mostrar mi vCard
    </Button>
  );
}

export default MyVCardButton;
