import * as React from 'react';
import {List, Avatar} from 'react-native-paper';
import {Alert, StyleSheet, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useState} from 'react';
import {db} from '../db/db';

function ContactListItem({contact, callbackContacts}) {
  /* ContactListItem is a touchable element that refers a contact name navigating to a vCard
   * image.*/
  const [deleteMode, setDeleteMode] = useState(false);
  const navigation = useNavigation();
  const contactName = contact.item.contact_name;
  const contactRole = contact.item.contact_role;
  const contactVCard = contact.item.contact_vcard;
  const contactId = contact.item.contact_id;
  const renderName = name => <Text>{name}</Text>;
  function onPressButton() {
    if (deleteMode) {
      Alert.alert('Elimino a ' + contactName + ' de su lista de contactos.');
      db.dbPointer.transaction(function (txn) {
        txn.executeSql(
          'DELETE FROM contacts WHERE contact_id = ?;',
          [contactId],
          null,
          null,
        );
      });
      callbackContacts();
    } else {
      navigation.push('vCardDetails', {
        userData: {
          contact_id: contactId,
          contact_name: contactName,
          contact_role: contactRole,
          contact_vcard: contactVCard,
        },
      });
    }
  }
  return (
    <List.Item
      title={renderName(contactName)}
      description={contactRole}
      left={props => (
        <Avatar.Image
          style={{alignSelf: 'center'}}
          source={{uri: contactVCard}}
        />
      )}
      right={props => {
        return deleteMode ? <List.Icon {...props} icon="delete" /> : null;
      }}
      onPress={() => onPressButton()}
      onLongPress={() => setDeleteMode(!deleteMode)}
      style={deleteMode ? styles.deleteButton : styles.normalButton}
    />
  );
}

const styles = StyleSheet.create({
  normalButton: {},
  deleteButton: {
    backgroundColor: '#ff4040',
  },
  thumbnail: {
    width: 494,
    height: 100,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});

export default ContactListItem;
