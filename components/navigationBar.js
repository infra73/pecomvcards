import React from 'react';
import {Appbar, Menu} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';

function CustomNavigationBar(props) {
  const navigation = useNavigation();
  const [visible, setVisible] = React.useState(false);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);
  /* CustomNavigationBar renders a nice header for all screens in the app.
   * it's extracted from https://callstack.github.io/react-native-paper/integrate-app-bar-with-react-navigation.html */
  return (
    <Appbar.Header>
      {props.back ? <Appbar.BackAction onPress={navigation.goBack} /> : null}
      <Appbar.Content
        title={
          props.route.params.userVcard
            ? 'Mi vCard'
            : props.route.params.userData
            ? props.route.params.userData.contact_name
            : props.route.params.emptyUser || props.route.params.editUser
            ? 'Editar vCard'
            : 'Otras vCard'
        }
        subtitle={
          props.route.params.userVcard
            ? props.route.params.userData.contact_name
            : props.route.params.userData
            ? props.route.params.userData.contact_role
            : null
        }
      />
      {!props.back ? (
        <Menu
          visible={visible}
          onDismiss={closeMenu}
          anchor={
            <Appbar.Action icon="menu" color="white" onPress={openMenu} />
          }>
          <Menu.Item
            onPress={() => {
              closeMenu();
              props.navigation.navigate('vCardAdd', {
                emptyUser: false,
                editUser: true,
              });
            }}
            title="Editar mi vCard"
          />
          <Menu.Item
            onPress={() => {
              closeMenu();
              props.navigation.navigate('Home', {
                userVcard: false,
                editUser: false,
              });
            }}
            title="Otras vCards"
          />
        </Menu>
      ) : null}
    </Appbar.Header>
  );
}

export default CustomNavigationBar;
