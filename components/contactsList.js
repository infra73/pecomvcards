import * as React from 'react';
import {FlatList, StyleSheet, Text} from 'react-native';
import ContactListItem from './contactListItem';

/*const myData = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'Carlos Maceira',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    name: 'Guillermo Muena',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    name: 'Alejandro Repetto',
  },
  {
    id: '6d7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'Nahuel Salazar',
  },
  {
    id: '7ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    name: 'Luis Bongianino',
  },
  {
    id: '88694a0f-3da1-471f-bd96-145571e29d72',
    name: 'Andrés Morozovsky',
  },
  {
    id: '9d7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'Carlos Rivera',
  },
  {
    id: 'aac68afc-c605-48d3-a4f8-fbd91aa97f63',
    name: 'Luciano Rossi',
  },
  {
    id: 'b8694a0f-3da1-471f-bd96-145571e29d72',
    name: 'Guido Miranda',
  },
  {
    id: 'cd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'Ignacio Martinez',
  },
  {
    id: 'dac68afc-c605-48d3-a4f8-fbd91aa97f63',
    name: 'Cristian Franco',
  },
  {
    id: 'e8694a0f-3da1-471f-bd96-145571e29d72',
    name: 'Lucia Maceira',
  },
];*/

const styles = StyleSheet.create({
  flatList: {
    flex: 1,
  },
});

function ContactList({contactsData, callbackContacts}) {
  /* ContactList is a container for list items that represents user available contacts*/
  const renderItem = contact => (
    <ContactListItem contact={contact} callbackContacts={callbackContacts} />
  );

  return (
    <FlatList
      style={styles.flatList}
      ListHeaderComponent={<Text />}
      data={contactsData}
      keyExtractor={item => item.contact_id.toString()}
      renderItem={renderItem}
      ListFooterComponent={<Text />}
    />
  );
}

export default ContactList;
