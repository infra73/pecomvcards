import React, {useEffect, useState} from 'react';
import {Alert, Image, StyleSheet, Text, View} from 'react-native';
import {TextInput, Title, Button} from 'react-native-paper';
import * as ImagePicker from 'react-native-image-picker';
import {db} from '../db/db';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  screenTitle: {
    alignSelf: 'center',
    paddingTop: 10,
  },
  textInput: {
    marginTop: 10,
  },
  stretch: {
    width: '70%',
    height: '70%',
    resizeMode: 'center',
    alignSelf: 'center',
  },
  image: {
    marginVertical: 24,
    alignItems: 'center',
  },
  imageSize: {
    width: 200,
    height: 200,
  },
});

function VCardsAdd({route, navigation}) {
  const [contactId, setContactId] = useState('');
  const [contactName, setContactName] = useState('');
  const [contactRole, setContactRole] = useState('');
  const [formName, setFormName] = useState('');
  const [formRole, setFormRole] = useState('');
  const [formVCard, setFormVCard] = useState('');
  const emptyUser = route.params.emptyUser;
  const editUser = route.params.editUser;

  useEffect(() => (editUser ? getUserData() : undefined), [editUser]);

  /* Opciones de Image Picker */
  const options = {
    selectionLimit: 1,
    mediaType: 'photo',
    includeBase64: true,
  };
  let imgSource;
  const onButtonPress = React.useCallback(() => {
    ImagePicker.launchImageLibrary(options, async response => {
      // console.log('Response = ', response);
      // Same code as in above section!
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else {
        // eslint-disable-next-line react-hooks/exhaustive-deps
        imgSource = response.assets[0].uri;
        setFormVCard(imgSource);
      }
    });
  }, [options]);
  const insertData = () => {
    if (formName === '' || formVCard === '') {
      Alert.alert('Nombre e Imagen son obligatorios');
      console.log('El usuario no puso nombre o imagen!');
    } else {
      db.dbPointer.transaction(function (tx) {
        tx.executeSql(
          emptyUser
            ? 'INSERT INTO user (contact_name, contact_role, contact_vcard) VALUES (?,?,?)'
            : 'INSERT INTO contacts (contact_name, contact_role, contact_vcard) VALUES (?,?,?)',
          [formName, formRole, formVCard],
          (tx, results) => {
            if (results.rowsAffected > 0) {
              if (emptyUser) {
                navigation.replace('vCardDetails', {
                  userVcard: true,
                  userData: {
                    contact_id: contactId,
                    contact_name: formName,
                    contact_role: formRole,
                    contact_vcard: formVCard,
                  },
                });
              } else {
                navigation.navigate('Home', {userVcard: false});
              }
            } else {
              Alert.alert('No se ha podido guardar el contacto');
            }
          },
        );
      });
    }
  };
  const getUserData = () => {
    db.dbPointer.transaction(tx => {
      tx.executeSql('SELECT * FROM user;', [], (tx, results) => {
        let temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        if (results.rows.length >= 1) {
          setContactId(temp[0].contact_id);
          setContactName(temp[0].contact_name);
          setContactRole(temp[0].contact_role);
          setFormVCard(temp[0].contact_vcard);
        }
      });
    });
  };
  const updateUserData = () => {
    db.dbPointer.transaction(tx => {
      if (formName === '' || formVCard === '') {
        Alert.alert('Nombre e Imagen son obligatorios');
        console.log('El usuario no puso nombre o imagen!');
      } else {
        tx.executeSql(
          'REPLACE INTO user(contact_id, contact_name, contact_role, contact_vcard) VALUES(?,?,?,?);',
          [contactId, formName, formRole, formVCard],
          () => {
            Alert.alert('Se ha actualizado su vCard');
            navigation.reset({
              index: 0,
              routes: [
                {
                  name: 'vCardDetails',
                  params: {
                    userVcard: true,
                    userData: {
                      contact_id: contactId,
                      contact_name: formName,
                      contact_role: formRole,
                      contact_vcard: formVCard,
                    },
                  },
                },
              ],
            });
          },
        );
      }
    });
  };

  function saveButtonAction() {
    if (editUser) {
      updateUserData();
    } else {
      insertData();
    }
  }

  return (
    <View style={styles.container}>
      <View>
        {emptyUser ? (
          <View>
            <Title style={styles.screenTitle}>
              Bienvenido, agregá tu vCard
            </Title>
          </View>
        ) : editUser ? (
          <Title style={styles.screenTitle}>{contactName}</Title>
        ) : (
          <Title style={styles.screenTitle}>Agregá una nueva vCard</Title>
        )}
        <TextInput
          mode={'outlined'}
          style={styles.textInput}
          label="Nombre"
          onChangeText={text => setFormName(text)}
        />
        <TextInput
          mode={'outlined'}
          style={styles.textInput}
          label="Puesto (opcional)"
          onChangeText={text => setFormRole(text)}
        />
        <Button
          style={styles.textInput}
          icon="camera"
          mode="contained"
          onPress={() => onButtonPress(options)}>
          Agregar imagen de vCard
        </Button>
      </View>
      <View>
        <Text> Vista Previa </Text>
        {formVCard ? (
          <View key={formVCard} style={styles.image}>
            <Image
              resizeMode="cover"
              resizeMethod="scale"
              style={styles.imageSize}
              source={{uri: formVCard}}
            />
          </View>
        ) : null}
      </View>
      <View>
        <Button
          style={styles.textInput}
          icon="content-save"
          mode="contained"
          onPress={() => saveButtonAction()}>
          Guardar
        </Button>
      </View>
    </View>
  );
}

export default VCardsAdd;
