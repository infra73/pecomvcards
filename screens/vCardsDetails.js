import React from 'react';
import {Image, StyleSheet, View} from 'react-native';

const styles = StyleSheet.create({
  view: {
    backgroundColor: '#102b52',
    flex: 1,
  },
  stretch: {
    width: 494,
    height: 644,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
});

function VCardsDetails({route}) {
  console.log('Llegue a details!');
  const contact_vcard = route.params.userData.contact_vcard;
  return (
    <View style={styles.view}>
      <Image style={styles.stretch} source={{uri: contact_vcard}} />
    </View>
  );
}

export default VCardsDetails;
