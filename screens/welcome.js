import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {db} from '../db/db';

const styles = StyleSheet.create({
  stretch: {
    width: 494,
    height: 644,
    resizeMode: 'center',
    alignSelf: 'center',
  },
  screen: {
    backgroundColor: '#1a315d',
    flex: 1,
    alignItems: 'center',
  },
});

const USER_NOT_EXISTS = 100;

function Welcome({navigation}) {
  const [user, setUser] = useState('');
  function splashScreenTimeout(time) {
    /* los parametros que se pasan a la ruta vCardDetails son:
     *  userVcard: le dice si es la vcard del usuario. Lo usa navigationBar para
     * saber si pone o no el menu.
     * userData: son los datos del usuario, tal y como salen en la tabla de la BD.
     * lo usa la vista detail para mostrar.*/
    if (user.contact_name) {
      // Verificacion para parar el renderizado hasta tener al user o nada.
      console.log('Me voy para Details');
      console.log(user);
      setTimeout(() => {
        navigation.reset({
          index: 0,
          routes: [
            {
              name: 'vCardDetails',
              params: {userVcard: true, userData: user},
            },
          ],
        });
      }, time);
    } else if (user === USER_NOT_EXISTS) {
      console.log('Me voy para Add');
      setTimeout(() => {
        navigation.reset({
          index: 0,
          routes: [
            {name: 'vCardAdd', params: {editUser: false, emptyUser: true}},
          ],
        });
      }, time);
    }
    return undefined;
  }

  splashScreenTimeout(1500);

  useEffect(() => {
    function getUserIfExists() {
      db.dbPointer.transaction(tx => {
        tx.executeSql('SELECT * FROM user;', [], (tx, results) => {
          let temp = [];
          for (let i = 0; i < results.rows.length; ++i) {
            temp.push(results.rows.item(i));
          }
          if (results.rows.length >= 1) {
            setUser(temp[0]);
          } else {
            setUser(USER_NOT_EXISTS);
          }
        });
      });
      return undefined;
    }
    getUserIfExists();
  }, []);

  return (
    <View style={styles.screen}>
      <Image
        style={styles.stretch}
        source={require('../images/pecomvCardsAppLogo.png')}
      />
    </View>
  );
}

export default Welcome;
