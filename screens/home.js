import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import ContactList from '../components/contactsList';
import {FAB} from 'react-native-paper';
import {useIsFocused} from '@react-navigation/native';
import {db} from '../db/db';

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 20,
    bottom: 50,
  },
  container: {
    flex: 1,
    overflow: 'hidden',
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
  },
  title: {
    alignItems: 'center',
  },
  message: {
    paddingTop: 200,
    fontSize: 20,
  },
});

function HomeScreen({navigation}) {
  console.log('Llegue a Home!');
  const [contacts, setContacts] = useState([]);
  const [emptyList, setemptyList] = useState(true);
  const isFocused = useIsFocused();
  // useEffect(() => {
  //   db.dbPointer.transaction(function (txn) {
  //     txn.executeSql(
  //       'CREATE TABLE IF NOT EXISTS contacts(contact_id INTEGER PRIMARY KEY AUTOINCREMENT, contact_name VARCHAR(40), contact_role VARCHAR(40), contact_vcard VARCHAR(255));',
  //       [],
  //     );
  //     txn.executeSql(
  //       'CREATE TABLE IF NOT EXISTS user (contact_id INTEGER PRIMARY KEY AUTOINCREMENT, contact_name VARCHAR(40), contact_role VARCHAR(40), contact_vcard VARCHAR(255));',
  //       [],
  //     );
  //   });
  // }, [isFocused]);
  useEffect(() => updateList(), [isFocused]);
  function updateList() {
    db.dbPointer.transaction(tx => {
      tx.executeSql('SELECT * FROM contacts;', [], (tx, results) => {
        let temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          temp.push(results.rows.item(i));
        }
        if (results.rows.length >= 1) {
          setemptyList(false);
          const sorted = [...temp].sort((a, b) => {
            return a.contact_name > b.contact_name ? 1 : -1;
          });
          setContacts(sorted);
        } else {
          setemptyList(true);
        }
      });
    });
    return undefined;
  }
  return (
    <View style={styles.container}>
      {emptyList ? (
        <View style={styles.title}>
          <Text style={styles.message}>
            ¡Parece que no tenés contactos todavía!
          </Text>
        </View>
      ) : (
        <ContactList contactsData={contacts} callbackContacts={updateList} />
      )}
      <FAB
        style={styles.fab}
        large
        icon="plus"
        onPress={() =>
          navigation.push('vCardAdd', {emptyUser: false, editUser: false})
        }
      />
    </View>
  );
}

export default HomeScreen;
