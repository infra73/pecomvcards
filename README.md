# Pecom vCards

## Dependencias ambiente de desarrollo
### Software
- nodeJS 16

### Android
- Android Studio, con el SDK.
- Un emulador o un telefono compatible
- Facebook Watchman.
- Mucha RAM!
- 

### Dependencias Internas
- react-native-paper
- react-native-vector-icons
- react-navigation/native
- react-native-screens

## iOs development installation
git clone
npm install
cd ios
pod install
cd ..


## Android Release instructions
.\gradlew clean
volver al directorio raiz
Subir el version code de .\android\app\build.gradle
react-native bundle --platform android --dev false --entry-file index.js --bundle-output .\android\app\src\main\assets\index.android.bundle --assets-dest .\android\app\build\intermediates\res\merged\release\
Verificar si quedo todo correcto corriendo por ultima vez el simulador
react-native run-android --variant=release
obtener la apk firmada (aab)
.\gradlew assembleRelease

## iOs Release instruction
Verificar Info.plist
Colocar los iconos de app en image.xassets
Verificar que en Copy Bundle Resources no esten las fuentes.
Verificar que aparezcan los archivos de iconos.
Cambiar el modo de Scheme a Release y el modo de compilación a Generic arm64.
