import SQLite from 'react-native-sqlite-storage';

/* Database Parameters */
SQLite.DEBUG(false);
SQLite.enablePromise(false);
const database_name = 'contacts.db';
const database_version = '1.0';
const database_displayname = 'contacts';
const database_size = 200000;
function errorCB(err) {
  console.log('SQL Error: ' + err);
}

function successCB() {
  console.log('SQL executed fine');
}
/* End Database Parameters*/

class appDatabase {
  constructor(dbPointer) {
    this.dbPointer = dbPointer;
    this.initDb();
  }
  initDb() {
    this.dbPointer.transaction(function (txn) {
      txn.executeSql(
        'CREATE TABLE IF NOT EXISTS contacts(contact_id INTEGER PRIMARY KEY AUTOINCREMENT, contact_name VARCHAR(40), contact_role VARCHAR(40), contact_vcard VARCHAR(255));',
        [],
      );
      txn.executeSql(
        'CREATE TABLE IF NOT EXISTS user (contact_id INTEGER PRIMARY KEY AUTOINCREMENT, contact_name VARCHAR(40), contact_role VARCHAR(40), contact_vcard VARCHAR(255));',
        [],
      );
    });
  }
  cleanDb() {
    this.dbPointer.transaction(function (txn) {
      txn.executeSql('DROP TABLE contacts;', []);
      txn.executeSql('DROP TABLE user;', []);
    });
  }
}

export const db = new appDatabase(
  SQLite.openDatabase(
    database_name,
    database_version,
    database_displayname,
    database_size,
    successCB,
    errorCB,
  ),
);
